CREATE TABLE IF NOT EXISTS user (
    id INTEGER PRIMARY KEY,
    username TEXT NOT NULL UNIQUE,
    hash TEXT NOT NULL,
    role TEXT NOT NULL,
    credential_max_id INTEGER NOT NULL
);
CREATE TABLE IF NOT EXIST credential(
    id INTEGER NOT NULL, 
    user_id INTEGER NOT NULL,

    content TEXT NOT NULL,

    PRIMARY KEY (id, user_id),
    FOREIGN KEY (user_id) REFERENCES user(id) ON DELETE CASCADE
);

# When creating user
INSERT INTO user (username, hash, role, credential_max_id) VALUES("daniel", "123", "USER", 1);

# When inserting credential (where user_id ID is 1)
SELECT credential_max_id from user where id=1;
UPDATE OR ROLLBACK user_id SET credential_max_id=2 WHERE id=1 AND credential_max_id=1;
INSERT INTO credential (id, user_id, content) VALUES(1, 1, "Content here!");
