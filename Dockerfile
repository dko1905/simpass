FROM gradle:7.4.2-jdk17-focal AS build
COPY --chown=gradle:gradle . /home/gradle/src
WORKDIR /home/gradle/src
RUN gradle build --no-daemon -x test

FROM eclipse-temurin:17-jre-focal
EXPOSE 8080
RUN mkdir /app
COPY --from=build /home/gradle/src/build/libs/*.jar /app/spring-boot-application.jar
WORKDIR /app
EXPOSE 8080
ENTRYPOINT ["java", "-jar", "./spring-boot-application.jar", "--spring.profiles.active=cloud"]
