package eu._0chaos.simpass.repository;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import eu._0chaos.simpass.exception.RecordExistsException;
import eu._0chaos.simpass.model.User;

import java.sql.SQLException;
import java.util.List;
import java.util.Optional;

@SpringBootTest
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
@ActiveProfiles("test")
public class UserRepositoryTests {
	private static final Logger logger = LoggerFactory.getLogger(UserRepositoryTests.class);
	@Autowired
	UserRepository userRepository;

	@BeforeEach
	@AfterAll
	public void clearDb() throws SQLException {
		for (final User user : userRepository.find()) {
			userRepository.delete(user.id());
		}
	}

	@Test
	public void testUniqueUsername() throws SQLException, RecordExistsException {
		boolean caught = false;
		User user = new User(0L, "oliver", "123", "USER", 0);
		user = userRepository.add(user);

		try {
			userRepository.add(user);
		} catch (RecordExistsException ex) {
			caught = true;
		} catch (SQLException ex) {
			logger.info("Error code: {}", ex.getMessage());
			logger.info(ex.toString());
		}

		assertTrue(caught);
	}

	@Test
	public void testFindById() throws SQLException, RecordExistsException {
		User user = new User(0L, "alex", "123", "USER", 0);
		user = userRepository.add(user);
		user = new User(0L, "oliver", "123", "USER", 0);
		user = userRepository.add(user);

		Optional<User> user1 = userRepository.findById(user.id());
		assertTrue(user1.isPresent());
		assertTrue(user.equals(user1.get()));
	}

	@Test
	public void testFindByUsername() throws SQLException, RecordExistsException {
		User user = new User(0L, "alex", "123", "USER", 0);
		user = userRepository.add(user);
		user = new User(0L, "oliver", "123", "USER", 0);
		user = userRepository.add(user);

		Optional<User> user1 = userRepository.findByUsername(user.username());
		assertTrue(user1.isPresent());
		assertEquals(user, user1.get());
	}

	@Test
	public void testFind() throws SQLException, RecordExistsException {
		User user1 = new User(0L, "oliver", "123", "USER", 0);
		User user2 = new User(0L, "rasmus", "321", "USER", 0);
		User user3 = new User(0L, "adam", "123", "USER", 0);

		user1 = userRepository.add(user1);
		user2 = userRepository.add(user2);
		user3 = userRepository.add(user3);

		List<User> users = userRepository.find();
		assertEquals(3, users.size());
		assertTrue(users.contains(user1));
		assertTrue(users.contains(user2));
		assertTrue(users.contains(user3));
	}

	@Test void testUpdate() throws SQLException, RecordExistsException {
		User user = new User(0L, "alex", "123", "USER", 0);
		user = userRepository.add(user);
		user = new User(0L, "oliver", "123", "USER", 0);
		user = userRepository.add(user);

		user = new User(user.id(), user.username(), "abc", user.role(), user.credentialMaxId());
		int updateCount = userRepository.update(user);
		assertTrue(updateCount > 0);

		Optional<User> user1 = userRepository.findById(user.id());
		assertTrue(user1.isPresent());
		assertEquals(user, user1.get());
	}

	@Test void testDelete() throws SQLException, RecordExistsException {
		User user = new User(0L, "alex", "123", "USER", 0);
		user = userRepository.add(user);
		user = new User(0L, "oliver", "123", "USER", 0);
		user = userRepository.add(user);

		int updateCount = userRepository.delete(user.id());
		assertTrue(updateCount > 0);

		Optional<User> user1 = userRepository.findById(user.id());
		assertTrue(user1.isEmpty());
	}
}
