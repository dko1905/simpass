package eu._0chaos.simpass.repository;

import static org.junit.jupiter.api.Assertions.*;

import eu._0chaos.simpass.exception.RecordExistsException;
import eu._0chaos.simpass.model.Credential;
import eu._0chaos.simpass.model.User;
import org.apache.commons.lang3.RandomStringUtils;
import org.junit.jupiter.api.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;

import java.sql.SQLException;
import java.util.List;
import java.util.Optional;

@SpringBootTest
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
@ActiveProfiles("test")
public class CredentialRepositoryTests {
	private static final Logger logger = LoggerFactory.getLogger(CredentialRepositoryTests.class);
	@Autowired
	CredentialRepository credentialRepository;
	@Autowired
	UserRepository userRepository;

	@BeforeEach
	@AfterAll
	public void clearDb() throws SQLException {
		for (final Credential credential : credentialRepository.find()) {
			credentialRepository.delete(credential.id(), credential.userId());
		}
		for (final User user : userRepository.find()) {
			userRepository.delete(user.id());
		}
	}

	private User createDummyUser() throws SQLException, RecordExistsException {
		String username = RandomStringUtils.randomAlphanumeric(5);
		User user = new User(0, username, "hash", "USER", 0);
		user = userRepository.add(user);

		return user;
	}

	@Test
	public void testTwoCredentials() throws SQLException, RecordExistsException {
		User user = createDummyUser();

		Credential credential = new Credential(0, user.id(), "content");
		credential = credentialRepository.add(credential);

		Credential credential1 = credentialRepository.add(credential);
		assertTrue(credential1.id() > credential.id());
	}

	@Test
	public void testTwoUsers() throws SQLException, RecordExistsException {
		User user = createDummyUser();
		User user1 = createDummyUser();

		Credential credential = new Credential(0, user.id(), "Hello");
		credential = credentialRepository.add(credential);
		assertEquals(credential.content(), "Hello");
		assertEquals(user.id(), credential.userId());

		Credential credential1 = new Credential(0, user1.id(), "World");
		credential1 = credentialRepository.add(credential1);
		assertEquals(credential1.content(), "World");
		assertEquals(user1.id(), credential1.userId());

		assertEquals(credential.id(), credential1.id());
		assertNotEquals(credential.userId(), credential1.userId());

		Optional<Credential> credential2 = credentialRepository.findByIdAndUserId(credential.id(), credential.userId());
		assertTrue(credential2.isPresent());
		assertEquals(credential, credential2.get());

		List<Credential> credentials = credentialRepository.findByUserId(credential.userId());
		assertEquals(1, credentials.size());
		assertEquals(credential, credentials.get(0));
	}

	@Test
	public void testDeletion() throws SQLException, RecordExistsException {
		User user = createDummyUser();
		User user1 = createDummyUser();

		Credential credential = new Credential(0, user.id(), "a");
		credential = credentialRepository.add(credential);

		Credential credential1 = new Credential(0, user1.id(), "b");
		credential1 = credentialRepository.add(credential1);

		assertEquals(2, credentialRepository.find().size());

		assertEquals(1, credentialRepository.delete(credential.id(), credential.userId()));
		assertEquals(1, credentialRepository.find().size());
		assertEquals(credential1, credentialRepository.find().get(0));
	}
}
