package eu._0chaos.simpass.controller;

import eu._0chaos.simpass.model.User;
import eu._0chaos.simpass.service.UserDetailsService.UserPrincipal;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Collection;

@RestController
public class AuthenticationController {
	private static final Logger logger = LoggerFactory.getLogger(AuthenticationController.class);

	@GetMapping("authenticate")
	public User authenticate(Authentication authentication) {
		Collection<? extends GrantedAuthority> authorities = authentication.getAuthorities();
		StringBuilder sb = new StringBuilder();

		sb.append("Authenticated ");
		sb.append(authentication.getName());
		sb.append(" (");
		for (final GrantedAuthority authority : authorities) {
			sb.append(authority.getAuthority());
			sb.append(",");
		}
		sb.append(")");
		logger.debug("{}", sb.toString());

		UserPrincipal userPrincipal = (UserPrincipal)authentication.getPrincipal();
		User user = userPrincipal.getUser();

		return user;
	}
}
