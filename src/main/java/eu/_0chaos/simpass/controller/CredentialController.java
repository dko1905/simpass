package eu._0chaos.simpass.controller;

import eu._0chaos.simpass.repository.CredentialRepository;
import eu._0chaos.simpass.repository.UserRepository;
import eu._0chaos.simpass.model.Credential;
import eu._0chaos.simpass.model.User;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;
import org.springframework.web.servlet.mvc.method.annotation.MvcUriComponentsBuilder;

import java.net.URI;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/user/{userId}/credential")
public class CredentialController {
	private static final Logger logger = LoggerFactory.getLogger(CredentialController.class);
	@Autowired
    CredentialRepository credentialRepository;
	@Autowired
    UserRepository userRepository;

	@PostMapping("/")
	public ResponseEntity<Credential> postCredential(Authentication authentication,
													 @PathVariable("userId") long userId,
													 @RequestBody Credential credential) {
		try {
			if (authentication.getAuthorities().contains(new SimpleGrantedAuthority("ADMIN"))) {
				Optional<User> user = userRepository.findById(userId);
				if (user.isEmpty()) throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Invalid user ID");

				credential = new Credential(credential.id(), userId, credential.content());
				credential = credentialRepository.add(credential);

				URI uri = MvcUriComponentsBuilder.fromController(getClass())
						.path("/credential/{id}")
						.buildAndExpand(credential.userId(), credential.id())
						.toUri();
				return ResponseEntity.created(uri).body(credential);
			} else if (authentication.getAuthorities().contains(new SimpleGrantedAuthority("USER"))) {
				Optional<User> user = userRepository.findByUsername(authentication.getName());
				if (user.isEmpty()) throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR);

				if (userId != user.get().id()) {
					throw new ResponseStatusException(HttpStatus.FORBIDDEN, "You cannot edit others credentials");
				}

				credential = new Credential(credential.id(), userId, credential.content());
				credential = credentialRepository.add(credential);

				URI uri = MvcUriComponentsBuilder.fromController(getClass())
						.path("/{id}")
						.buildAndExpand(credential.userId(), credential.id())
						.toUri();
				return ResponseEntity.created(uri).body(credential);
			} else {
				throw new ResponseStatusException(HttpStatus.FORBIDDEN);
			}
		} catch (ResponseStatusException ex) {
			throw ex;
		} catch (Exception ex) {
			logger.error(ex.getMessage());
			throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, ex.getMessage());
		}
	}

	@GetMapping("/{id}")
	public ResponseEntity<Credential> getOneCredential(Authentication authentication,
													@PathVariable("userId") long userId,
													@PathVariable("id") long id) {
		try {
			if (authentication.getAuthorities().contains(new SimpleGrantedAuthority("ADMIN"))) {
				Optional<User> user = userRepository.findById(userId);
				if (user.isEmpty()) throw new ResponseStatusException(HttpStatus.NOT_FOUND, "User not found");

				Optional<Credential> credential = credentialRepository.findByIdAndUserId(id, userId);
				if (credential.isEmpty()) throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Credential not found");
				return ResponseEntity.ok(credential.get());
			} else if (authentication.getAuthorities().contains(new SimpleGrantedAuthority("USER"))) {
				Optional<User> user = userRepository.findByUsername(authentication.getName());
				if (user.isEmpty()) throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR);

				if (userId != user.get().id()) {
					throw new ResponseStatusException(HttpStatus.FORBIDDEN, "You cannot read others credentials");
				}

				Optional<Credential> credential = credentialRepository.findByIdAndUserId(id, userId);
				if (credential.isEmpty()) throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Credential not found");
				return ResponseEntity.ok(credential.get());
			} else {
				throw new ResponseStatusException(HttpStatus.FORBIDDEN);
			}
		} catch (ResponseStatusException ex) {
			throw ex;
		} catch (Exception ex) {
			logger.error(ex.getMessage());
			throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, ex.getMessage());
		}
	}

	@GetMapping("/")
	public ResponseEntity<List<Credential>> getAllCredentials(Authentication authentication,
															  @PathVariable("userId") long userId) {
		try {
			if (authentication.getAuthorities().contains(new SimpleGrantedAuthority("ADMIN"))) {
				Optional<User> user = userRepository.findById(userId);
				if (user.isEmpty()) throw new ResponseStatusException(HttpStatus.NOT_FOUND, "User not found");

				return ResponseEntity.ok(credentialRepository.findByUserId(userId));
			} else if (authentication.getAuthorities().contains(new SimpleGrantedAuthority("USER"))) {
				Optional<User> user = userRepository.findByUsername(authentication.getName());
				if (user.isEmpty()) throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR);

				if (userId != user.get().id()) {
					throw new ResponseStatusException(HttpStatus.FORBIDDEN, "You cannot read others credentials");
				}

				return ResponseEntity.ok(credentialRepository.findByUserId(userId));
			} else {
				throw new ResponseStatusException(HttpStatus.FORBIDDEN);
			}
		} catch (ResponseStatusException ex) {
			throw ex;
		} catch (Exception ex) {
			logger.error(ex.getMessage());
			throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, ex.getMessage());
		}
	}

	@PutMapping("/{id}")
	public ResponseEntity<Credential> putCredential(Authentication authentication,
													@PathVariable("userId") long userId,
													@PathVariable("id") long id,
													@RequestBody Credential credential) {
		try {
			if (authentication.getAuthorities().contains(new SimpleGrantedAuthority("ADMIN"))) {
				Optional<User> user = userRepository.findByUsername(authentication.getName());
				if (user.isEmpty()) throw new ResponseStatusException(HttpStatus.NOT_FOUND, "User not found");

				credential = new Credential(id, userId, credential.content());
				int rows = credentialRepository.update(credential);
				if (rows == 0) {
					throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Credential not found");
				}

				return ResponseEntity.ok(credential);
			} else if (authentication.getAuthorities().contains(new SimpleGrantedAuthority("USER"))) {
				Optional<User> user = userRepository.findByUsername(authentication.getName());
				if (user.isEmpty()) throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR);

				if (userId != user.get().id()) {
					throw new ResponseStatusException(HttpStatus.FORBIDDEN, "You cannot edit others credentials");
				}

				credential = new Credential(id, userId, credential.content());
				int rows = credentialRepository.update(credential);
				if (rows == 0) {
					throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Credential not found");
				}

				return ResponseEntity.ok(credential);
			} else {
				throw new ResponseStatusException(HttpStatus.FORBIDDEN);
			}
		} catch (ResponseStatusException ex) {
			throw ex;
		} catch (Exception ex) {
			logger.error(ex.getMessage());
			throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, ex.getMessage());
		}
	}

	@DeleteMapping("/{id}")
	public ResponseEntity<Credential> deleteCredential(Authentication authentication,
													   @PathVariable("userId") long userId,
													   @PathVariable("id") long id) {
		try {
			if (authentication.getAuthorities().contains(new SimpleGrantedAuthority("ADMIN"))) {
				Optional<User> user = userRepository.findByUsername(authentication.getName());
				if (user.isEmpty()) throw new ResponseStatusException(HttpStatus.NOT_FOUND, "User not found");

				int rows = credentialRepository.delete(id, userId);
				if (rows == 0) {
					throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Credential not found");
				}

				return ResponseEntity.noContent().build();
			} else if (authentication.getAuthorities().contains(new SimpleGrantedAuthority("USER"))) {
				Optional<User> user = userRepository.findByUsername(authentication.getName());
				if (user.isEmpty()) throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR);

				if (userId != user.get().id()) {
					throw new ResponseStatusException(HttpStatus.FORBIDDEN, "You cannot edit others credentials");
				}

				int rows = credentialRepository.delete(id, userId);
				if (rows == 0) {
					throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Credential not found");
				}

				return ResponseEntity.noContent().build();
			} else {
				throw new ResponseStatusException(HttpStatus.FORBIDDEN);
			}
		} catch (ResponseStatusException ex) {
			throw ex;
		} catch (Exception ex) {
			logger.error(ex.getMessage());
			throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, ex.getMessage());
		}
	}
}
