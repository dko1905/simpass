package eu._0chaos.simpass.controller;

import eu._0chaos.simpass.repository.UserRepository;
import eu._0chaos.simpass.exception.RecordExistsException;
import eu._0chaos.simpass.model.User;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;
import org.springframework.web.servlet.mvc.method.annotation.MvcUriComponentsBuilder;

import java.net.URI;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/user")
public class UserController {
	private static final Logger logger = LoggerFactory.getLogger(UserController.class);
	@Autowired
    UserRepository userRepository;
	@Autowired
	PasswordEncoder passwordEncoder;

	@PostMapping("/")
	public ResponseEntity<User> postUser(Authentication authentication, @RequestBody User inputUser,
										 @RequestParam boolean hashPassword) {
		try {
			if (authentication.getAuthorities().contains(new SimpleGrantedAuthority("ADMIN"))) {
				if (hashPassword) {
					String hash = passwordEncoder.encode(inputUser.hash());
					inputUser = new User(inputUser.id(), inputUser.username(), hash, inputUser.role(), inputUser.credentialMaxId());
				}
				User updatedUser = userRepository.add(inputUser);
				URI uri = MvcUriComponentsBuilder.fromController(getClass())
						.path("/{id}")
						.buildAndExpand(updatedUser.id())
						.toUri();
				return ResponseEntity.created(uri).body(updatedUser);
			} else {
				throw new ResponseStatusException(HttpStatus.FORBIDDEN);
			}
		} catch (ResponseStatusException ex) {
			throw ex;
		} catch (RecordExistsException ex) {
			throw new ResponseStatusException(HttpStatus.CONFLICT, ex.getMessage());
		} catch (Exception ex) {
			logger.error(ex.getMessage());
			throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, ex.getMessage());
		}
	}

	@GetMapping("/")
	public List<User> getUsers(Authentication authentication) {
		// Only allow user to see themselves and the admin to see everything.
		try {
			if (authentication.getAuthorities().contains(new SimpleGrantedAuthority("ADMIN"))) {
				return userRepository.find();
			} else {
				return List.of(userRepository.findByUsername(authentication.getName()).get());
			}
		} catch (Exception ex) {
			logger.error(ex.getMessage());
			throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, ex.getMessage());
		}
	}

	@GetMapping("/{id}")
	public User getUserById(Authentication authentication, @PathVariable("id") long id) {
		try {
			if (authentication.getAuthorities().contains(new SimpleGrantedAuthority("ADMIN"))) {
				Optional<User> user = userRepository.findById(id);
				if (user.isEmpty()) {
					throw new ResponseStatusException(HttpStatus.NOT_FOUND);
				} else {
					return user.get();
				}
			} else {
				Optional<User> user = userRepository.findById(id);
				if (user.isEmpty() || !user.get().username().equals(authentication.getName())) {
					throw new ResponseStatusException(HttpStatus.NOT_FOUND);
				} else {
					return user.get();
				}
			}
		} catch (ResponseStatusException ex) {
			throw ex;
		} catch (Exception ex) {
			logger.error(ex.getMessage());
			throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, ex.getMessage());
		}
	}

	@PutMapping("/{id}")
	public ResponseEntity<User> putUser(Authentication authentication, @PathVariable("id") long id,
										@RequestBody User patchedUser, @RequestParam boolean hashPassword) {
		try {
			if (authentication.getAuthorities().contains(new SimpleGrantedAuthority("ADMIN"))) {
				Optional<User> user = userRepository.findById(id);
				if (user.isEmpty()) {
					throw new ResponseStatusException(HttpStatus.NOT_FOUND);
				} else {
					User returnUser = user.get();
					String username, hash, role;
					if (patchedUser.username() != null) {
						username = patchedUser.username();
					} else {
						username = returnUser.username();
					}
					if (patchedUser.hash() != null) {
						if (hashPassword) {
							hash = passwordEncoder.encode(patchedUser.hash());
						} else {
							hash = patchedUser.hash();
						}
					} else {
						hash = returnUser.hash();
					}
					if (patchedUser.role() != null) {
						role = patchedUser.role();
					} else {
						role = returnUser.role();
					}

					returnUser = new User(returnUser.id(), username, hash, role, returnUser.credentialMaxId());
					// Allow ADMIN to change anything he likes, we trust him not to break the program.
					int rows = userRepository.update(returnUser);
					if (rows == 0) {
						throw new ResponseStatusException(HttpStatus.CONFLICT, "Updated before UPDATE");
					} else {
						return ResponseEntity.ok(returnUser);
					}
				}
			} else {
				Optional<User> user = userRepository.findById(id);
				if (user.isEmpty() || !user.get().username().equals(authentication.getName())) {
					throw new ResponseStatusException(HttpStatus.NOT_FOUND);
				} else {
					User existingUser = user.get();
					String username = existingUser.username(), hash = existingUser.hash(), role = existingUser.role();

					if (patchedUser.username() != null) {
						username = patchedUser.username();
					}
					if (patchedUser.hash() != null) {
						if (hashPassword) {
							hash = passwordEncoder.encode(patchedUser.hash());
						} else {
							hash = patchedUser.hash();
						}
					}
					User updatedUser = new User(existingUser.id(), username, hash, role, existingUser.credentialMaxId());

					// We only allow normal users to change username and password.
					int rows = userRepository.update(updatedUser);
					if (rows == 0) {
						throw new ResponseStatusException(HttpStatus.CONFLICT, "Updated before UPDATE");
					} else {
						return ResponseEntity.ok(updatedUser);
					}
				}
			}
		} catch (ResponseStatusException ex) {
			throw ex;
		} catch (Exception ex) {
			logger.error(ex.getMessage());
			throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, ex.getMessage());
		}
	}

	@DeleteMapping("/{id}")
	public ResponseEntity deleteUser(Authentication authentication, @PathVariable("id") long id) {
		try {
			if (authentication.getAuthorities().contains(new SimpleGrantedAuthority("ADMIN"))) {
				int rowsUpdated = userRepository.delete(id);
				if (rowsUpdated < 1) {
					throw new ResponseStatusException(HttpStatus.NOT_FOUND);
				} else {
					return ResponseEntity.noContent().build();
				}
			} else {
				Optional<User> user = userRepository.findById(id);
				if (user.isEmpty() || !user.get().username().equals(authentication.getName())) {
					throw new ResponseStatusException(HttpStatus.NOT_FOUND);
				} else {
					int rowsUpdated = userRepository.delete(id);
					if (rowsUpdated < 1) {
						throw new ResponseStatusException(HttpStatus.CONFLICT, "Deleted before DELETE");
					} else {
						return ResponseEntity.noContent().build();
					}
				}
			}
		} catch (ResponseStatusException ex) {
			throw ex;
		} catch (Exception ex) {
			logger.error(ex.getMessage());
			throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, ex.getMessage());
		}
	}
}
