package eu._0chaos.simpass;

import eu._0chaos.simpass.model.User;
import eu._0chaos.simpass.repository.UserRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.security.crypto.password.PasswordEncoder;

import java.util.List;

@SpringBootApplication
public class SimpassApplication implements CommandLineRunner {
	private static final Logger logger = LoggerFactory.getLogger(SimpassApplication.class);
	@Autowired
	UserRepository userRepository;
	@Autowired
	PasswordEncoder passwordEncoder;

	public static void main(String[] args) {
		SpringApplication.run(SimpassApplication.class, args);
	}

	private boolean containsUsername(List<User> input, String username) {
		return input
				.stream()
				.anyMatch(user -> user.username().equals(username));
	}

	@Override
	public void run(String... args) throws Exception {
		List<User> currentUsers = userRepository.find();
		if (!containsUsername(currentUsers, "daniel")) {
			User user = new User(0L, "daniel", passwordEncoder.encode("123"), "USER", 0);
			userRepository.add(user);
		}
		if (!containsUsername(currentUsers, "admin")) {
			User user = new User(0L, "admin", passwordEncoder.encode("123"), "USER,ADMIN", 0);
			userRepository.add(user);
		}
	}
}
