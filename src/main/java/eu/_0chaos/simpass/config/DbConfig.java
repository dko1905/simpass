package eu._0chaos.simpass.config;

import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;
import eu._0chaos.simpass.repository.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.sqlite.SQLiteConfig;
import org.sqlite.SQLiteDataSource;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.Objects;

@Configuration
public class DbConfig {
	private static final Logger logger = LoggerFactory.getLogger(DbConfig.class);
	@Value("${database.backend}")
	private String databaseBackend = null;
	@Value("${database.username}")
	private String username = null;
	@Value("${database.password}")
	private String password = null;
	@Value("${database.jdbcUrl}")
	private String dataSourceUrl = null;

	@Nonnull
	private String nullOr(@Nullable String val1, @Nonnull String or) {
		if (val1 != null) return val1;
		return or;
	}

	public DataSource sqliteDataSourceProvider() throws SQLException {
		// Load dynamic library
		try {
			Class.forName("org.sqlite.JDBC");
		} catch (Exception ex) {
			String message = ex.getMessage();

			if (message.equals("")) message = ex.toString();

			logger.error("SQLite class not found: %s", message);
			throw new RuntimeException(ex);
		}
		// Configure SQLite and Hikari
		SQLiteConfig sqliteConfig = new SQLiteConfig();
		sqliteConfig.enforceForeignKeys(true);
		sqliteConfig.setEncoding(SQLiteConfig.Encoding.UTF8);

		SQLiteDataSource sqliteDataSource = new SQLiteDataSource(sqliteConfig);
		sqliteDataSource.setUrl(nullOr(dataSourceUrl, "jdbc:sqlite:database.db"));

		HikariConfig config = new HikariConfig();
		config.setDataSource(sqliteDataSource);

		HikariDataSource dataSource = new HikariDataSource(config);
		// Create database & tables
		try (Connection connection = dataSource.getConnection()) {
			try (PreparedStatement preparedStatement = connection.prepareStatement("" +
					"CREATE TABLE IF NOT EXISTS user (" +
					"    id INTEGER PRIMARY KEY," +
					"    username TEXT NOT NULL UNIQUE," +
					"    hash TEXT NOT NULL," +
					"    role TEXT NOT NULL," +
					"    credential_max_id INTEGER NOT NULL" +
					");"
			)) {
				preparedStatement.execute();
			}
			try (PreparedStatement preparedStatement = connection.prepareStatement("" +
					"CREATE TABLE IF NOT EXISTS credential(" +
					"    id INTEGER NOT NULL," +
					"    user_id INTEGER NOT NULL," +
					"" +
					"    content TEXT NOT NULL," +
					"" +
					"    PRIMARY KEY (id, user_id)," +
					"    FOREIGN KEY (user_id) REFERENCES user(id) ON DELETE CASCADE" +
					");"
			)) {
				preparedStatement.execute();
			}
		}

		return dataSource;
	}

	public DataSource postgresDataSourceProvider() throws SQLException {
		HikariConfig config = new HikariConfig();
		config.setJdbcUrl(dataSourceUrl);
		config.setUsername(username);
		config.setPassword(password);
		config.addDataSourceProperty("cachePrepStmts", "true");
		config.addDataSourceProperty( "prepStmtCacheSize" , "250" );
		config.addDataSourceProperty( "prepStmtCacheSqlLimit" , "2048" );
		HikariDataSource dataSource = new HikariDataSource(config);

		// Table creation
		try (Connection connection = dataSource.getConnection()) {
			try (PreparedStatement preparedStatement = connection.prepareStatement("" +
					"CREATE TABLE IF NOT EXISTS simpass_user(" +
					"    id SERIAL PRIMARY KEY," +
					"    username TEXT NOT NULL UNIQUE," +
					"    hash TEXT NOT NULL," +
					"    role TEXT NOT NULL," +
					"    credential_max_id INTEGER NOT NULL" +
					");"
			)) {
				preparedStatement.execute();
			}
			try (PreparedStatement preparedStatement = connection.prepareStatement("" +
					"CREATE TABLE IF NOT EXISTS simpass_credential(" +
					"    id INTEGER NOT NULL," +
					"    user_id INTEGER NOT NULL," +
					"" +
					"    content TEXT NOT NULL," +
					"" +
					"    PRIMARY KEY (id, user_id)," +
					"    FOREIGN KEY (user_id) REFERENCES simpass_user(id) ON DELETE CASCADE" +
					");"
			)) {
				preparedStatement.execute();
			}
		}

		return dataSource;
	}

	@Bean
	public DataSource dataSourceProvider() throws SQLException {
		if (databaseBackend.equals("postgres"))
			return postgresDataSourceProvider();
		else
			return sqliteDataSourceProvider();
	}

	@Bean
	public UserRepository userRepositoryProvider() {
		if (databaseBackend.equals("postgres"))
			return new UserRepositoryPostgresImpl();
		else
			return new UserRepositorySQLiteImpl();
	}

	@Bean
	public CredentialRepository credentialRepositoryProvider() {
		if (databaseBackend.equals("postgres"))
			return new CredentialRepositoryPostgresImpl();
		else
			return new CredentialRepositorySQLiteImpl();
	}
}
