package eu._0chaos.simpass.model;

public record Credential (
	long id,
	long userId,
	String content
) {}
