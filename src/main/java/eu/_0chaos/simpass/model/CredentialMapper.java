package eu._0chaos.simpass.model;

import javax.annotation.Nonnull;
import java.sql.ResultSet;
import java.sql.SQLException;

public class CredentialMapper {
	@Nonnull
	public static Credential mapRow(@Nonnull ResultSet rs) throws SQLException {
		return new Credential(
				rs.getLong("id"),
				rs.getLong("user_id"),
				rs.getString("content")
		);
	}
}
