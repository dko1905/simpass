package eu._0chaos.simpass.model;

public record User (
	long id,

	String username,
	String hash,

	String role,
	long credentialMaxId
) {}
