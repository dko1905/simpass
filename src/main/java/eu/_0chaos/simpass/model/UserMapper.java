package eu._0chaos.simpass.model;

import javax.annotation.Nonnull;
import java.sql.ResultSet;
import java.sql.SQLException;

public class UserMapper {
	@Nonnull
	public static User mapRow(@Nonnull ResultSet rs) throws SQLException {
		return new User(
				rs.getLong("id"),
				rs.getString("username"),
				rs.getString("hash"),
				rs.getString("role"),
				rs.getLong("credential_max_id")
		);
	}
}
