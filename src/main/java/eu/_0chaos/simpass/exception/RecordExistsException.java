package eu._0chaos.simpass.exception;

public class RecordExistsException extends Exception {
	public RecordExistsException(String message) {
		super(message);
	}
	public RecordExistsException(String message, Throwable cause) {
		super(message, cause);
	}
}
