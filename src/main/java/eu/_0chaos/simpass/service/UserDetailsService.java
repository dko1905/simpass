package eu._0chaos.simpass.service;

import eu._0chaos.simpass.repository.UserRepository;
import eu._0chaos.simpass.model.User;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.sql.SQLException;
import java.util.Arrays;
import java.util.Collection;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class UserDetailsService implements org.springframework.security.core.userdetails.UserDetailsService {
	private static final Logger logger = LoggerFactory.getLogger(UserDetailsService.class);
	@Autowired
    UserRepository userRepository;

	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		try {
			Optional<User> user = userRepository.findByUsername(username);
			if (user.isEmpty()) {
				throw new UsernameNotFoundException(username);
			}
			return new UserPrincipal(user.get());
		} catch (SQLException ex) {
			logger.error("Caught while loading user by name: %s", ex.getMessage());
			throw new UsernameNotFoundException(username, ex);
		}
	}

	public class UserPrincipal implements UserDetails {
		private User user;

		public UserPrincipal(User user) {
			this.user = user;
		}

		@Override
		public Collection<? extends GrantedAuthority> getAuthorities() {
			return Arrays.stream(user.role().split(",", 20))
					.map(SimpleGrantedAuthority::new)
					.collect(Collectors.toList());
		}

		@Override
		public String getPassword() {
			return user.hash();
		}

		@Override
		public String getUsername() {
			return user.username();
		}

		@Override
		public boolean isAccountNonExpired() {
			return true;
		}

		@Override
		public boolean isAccountNonLocked() {
			return true;
		}

		@Override
		public boolean isCredentialsNonExpired() {
			return true;
		}

		@Override
		public boolean isEnabled() {
			return true;
		}

		public User getUser() {
			return user;
		}
	}
}
