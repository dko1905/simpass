package eu._0chaos.simpass.repository;

import eu._0chaos.simpass.model.Credential;
import eu._0chaos.simpass.model.CredentialMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.annotation.Nonnull;
import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class CredentialRepositoryPostgresImpl implements CredentialRepository {
    private static final Logger logger = LoggerFactory.getLogger(CredentialRepositoryPostgresImpl.class);
    @Autowired
    public DataSource dataSource;

    @Override
    public Credential add(@Nonnull Credential credential) throws SQLException {
        try (Connection connection = dataSource.getConnection();
             PreparedStatement preparedStatement = connection.prepareStatement("" +
                     "SELECT credential_max_id FROM simpass_user WHERE id=?;");
             PreparedStatement preparedStatement1 = connection.prepareStatement("" +
                     "UPDATE simpass_user SET credential_max_id=? WHERE id=? AND credential_max_id=?;");
             PreparedStatement preparedStatement2 = connection.prepareStatement("" +
                     "INSERT INTO simpass_credential (id, user_id, content) VALUES(?, ?, ?);");
             PreparedStatement preparedStatement3 = connection.prepareStatement("" +
                     "SELECT lastval();")) {
            connection.setAutoCommit(false);
            try {
                preparedStatement.setLong(1, credential.userId());
                ResultSet rs = preparedStatement.executeQuery();
                if (!rs.next()) throw new SQLException("Failed to select credential_max_id");
                long maxId = rs.getLong(1);

                maxId += 1;
                preparedStatement1.setLong(1, maxId);
                preparedStatement1.setLong(2, credential.userId());
                preparedStatement1.setLong(3, maxId - 1);

                int rows = preparedStatement1.executeUpdate();
                if (rows < 1) throw new SQLException("Failed to update credential_max_id");

                credential = new Credential(maxId, credential.userId(), credential.content());
                preparedStatement2.setLong(1, credential.id());
                preparedStatement2.setLong(2, credential.userId());
                preparedStatement2.setString(3, credential.content());
                rows = preparedStatement2.executeUpdate();
                if (rows < 1) throw new SQLException(String.format("Failed to insert credential (%s)", credential));

                connection.commit();
                return credential;
            } catch (SQLException ex) {
                if (ex.getMessage().contains("duplicate")) {
                    logger.error("BUG: Identical credential already exists: {}", credential.toString());
                }
                throw ex;
            } catch (Exception ex) {
                connection.rollback();
                throw ex;
            }
        }
    }

    @Override
    public List<Credential> find() throws SQLException {
        try (Connection connection = dataSource.getConnection();
             PreparedStatement preparedStatement = connection.prepareStatement("" +
                     "SELECT id, user_id, content FROM simpass_credential;")) {
            ResultSet rs = preparedStatement.executeQuery();
            ArrayList<Credential> credentials = new ArrayList<>();

            while (rs.next()) {
                credentials.add(CredentialMapper.mapRow(rs));
            }

            return credentials;
        }
    }

    @Override
    public List<Credential> findByUserId(long userId) throws SQLException {
        try (Connection connection = dataSource.getConnection();
             PreparedStatement preparedStatement = connection.prepareStatement("" +
                     "SELECT id, user_id, content FROM simpass_credential WHERE user_id=?;")) {
            preparedStatement.setLong(1, userId);
            ResultSet rs = preparedStatement.executeQuery();
            ArrayList<Credential> credentials = new ArrayList<>();

            while (rs.next()) {
                credentials.add(CredentialMapper.mapRow(rs));
            }

            return credentials;
        }
    }

    @Override
    public Optional<Credential> findByIdAndUserId(long id, long userId) throws SQLException {
        try (Connection connection = dataSource.getConnection();
             PreparedStatement preparedStatement = connection.prepareStatement("" +
                     "SELECT id, user_id, content FROM simpass_credential WHERE id=? AND user_id=?;")) {
            preparedStatement.setLong(1, id);
            preparedStatement.setLong(2, userId);
            ResultSet rs = preparedStatement.executeQuery();

            if (!rs.next()) return Optional.empty();
            return Optional.of(CredentialMapper.mapRow(rs));
        }
    }

    @Override
    public int update(@Nonnull Credential credential) throws SQLException {
        try (Connection connection = dataSource.getConnection();
             PreparedStatement preparedStatement = connection.prepareStatement("" +
                     "UPDATE simpass_credential SET content=? WHERE id=? AND user_id=?;")) {
            preparedStatement.setString(1, credential.content());
            preparedStatement.setLong(2, credential.id());
            preparedStatement.setLong(3, credential.userId());
            return preparedStatement.executeUpdate();
        }
    }

    @Override
    public int delete(long id, long userId) throws SQLException {
        try (Connection connection = dataSource.getConnection();
             PreparedStatement preparedStatement = connection.prepareStatement("" +
                     "DELETE FROM simpass_credential WHERE id=? AND user_id=?;")) {
            preparedStatement.setLong(1, id);
            preparedStatement.setLong(2, userId);
            return preparedStatement.executeUpdate();
        }
    }
}
