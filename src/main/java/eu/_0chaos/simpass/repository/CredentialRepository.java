package eu._0chaos.simpass.repository;

import eu._0chaos.simpass.model.Credential;
import org.springframework.stereotype.Repository;

import javax.annotation.Nonnull;
import java.sql.SQLException;
import java.util.List;
import java.util.Optional;

@Repository
public interface CredentialRepository {
	/**
	 * Add credential to repository.
	 * @param credential Credential to add
	 * @return A new credential with the ID set
	 * @throws SQLException Generic database exception
	 */
	public Credential add(@Nonnull Credential credential) throws SQLException;

	/**
	 * Find all credentials in repository.
	 * @return List of credentials, might be 0 in length.
	 * @throws SQLException Generic database exception
	 */
	public List<Credential> find() throws SQLException;

	/**
	 * Find all credentials belonging to a user.
	 * @param userId ID of user to use
	 * @return List of credentials belonging to user
	 * @throws SQLException Generic database exception
	 */
	public List<Credential> findByUserId(long userId) throws SQLException;

	/**
	 * Find a credential from a specific user using its ID.
	 * @param id ID of the credential to find
	 * @param userId ID of user who owns the credential
	 * @return A credential if found, nothing if not found
	 * @throws SQLException Generic database exception
	 */
	public Optional<Credential> findByIdAndUserId(long id, long userId) throws SQLException;

	/**
	 * Update an existing credential using its ID.
	 * @param credential Credential containing its ID and information to be updated
	 * @return Nr. of rows updated
	 * @throws SQLException Generic database exception
	 */
	public int update(@Nonnull Credential credential) throws SQLException;

	/**
	 * Delete credential using its ID.
	 * @param id ID of user to delete
	 * @param userId ID of user to delete credential from
	 * @return Nr. of rows updated
	 * @throws SQLException Generic database exception
	 */
	public int delete(long id, long userId) throws SQLException;
}
