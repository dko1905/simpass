package eu._0chaos.simpass.repository;

import eu._0chaos.simpass.exception.RecordExistsException;
import eu._0chaos.simpass.model.User;
import eu._0chaos.simpass.model.UserMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.annotation.Nonnull;
import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class UserRepositoryPostgresImpl implements UserRepository {
    @Autowired
    public DataSource dataSource;

    @Nonnull
    @Override
    public User add(@Nonnull User inputUser) throws SQLException, RecordExistsException {
        try (Connection connection = dataSource.getConnection();
             PreparedStatement preparedStatement = connection.prepareStatement("" +
                     "INSERT INTO simpass_user(username, hash, role, credential_max_id) VALUES(?, ?, ?, ?);");
             PreparedStatement preparedStatement1 = connection.prepareStatement("" +
                     "SELECT lastval();")) {
            connection.setAutoCommit(false);
            try {
                preparedStatement.setString(1, inputUser.username());
                preparedStatement.setString(2, inputUser.hash());
                preparedStatement.setString(3, inputUser.role());
                preparedStatement.setLong(4, inputUser.credentialMaxId());
                preparedStatement.execute();

                ResultSet rs = preparedStatement1.executeQuery();
                rs.next();
                User addedUser = new User(
                        rs.getLong(1),
                        inputUser.username(),
                        inputUser.hash(),
                        inputUser.role(),
                        inputUser.credentialMaxId()
                );

                connection.commit();
                return addedUser;
            } catch (SQLException ex) {
                if (ex.getMessage().contains("duplicate")) {
                    throw new RecordExistsException("User with same ID already exists");
                }
                throw ex;
            } catch (Exception ex) {
                connection.rollback();
                throw ex;
            }
        }
    }

    @Nonnull
    @Override
    public List<User> find() throws SQLException {
        try (Connection connection = dataSource.getConnection();
             PreparedStatement preparedStatement = connection.prepareStatement("" +
                     "SELECT id, username, hash, role, credential_max_id FROM simpass_user;")) {
            ResultSet rs = preparedStatement.executeQuery();
            ArrayList<User> users = new ArrayList<>();

            while (rs.next()) {
                users.add(UserMapper.mapRow(rs));
            }

            return users;
        }
    }

    @Nonnull
    @Override
    public Optional<User> findById(long id) throws SQLException {
        try (Connection connection = dataSource.getConnection();
             PreparedStatement preparedStatement = connection.prepareStatement("" +
                     "SELECT id, username, hash, role, credential_max_id FROM simpass_user WHERE id=?;")) {
            preparedStatement.setLong(1, id);

            ResultSet rs = preparedStatement.executeQuery();
            if (rs.next()) {
                return Optional.of(UserMapper.mapRow(rs));
            } else {
                return Optional.empty();
            }
        }
    }

    @Nonnull
    @Override
    public Optional<User> findByUsername(@Nonnull String username) throws SQLException {
        try (Connection connection = dataSource.getConnection();
             PreparedStatement preparedStatement = connection.prepareStatement("" +
                     "SELECT id, username, hash, role, credential_max_id FROM simpass_user WHERE username=?;")) {
            preparedStatement.setString(1, username);

            ResultSet rs = preparedStatement.executeQuery();
            if (rs.next()) {
                return Optional.of(UserMapper.mapRow(rs));
            } else {
                return Optional.empty();
            }
        }
    }

    @Nonnull
    @Override
    public int update(@Nonnull User user) throws SQLException {
        try (Connection connection = dataSource.getConnection();
             PreparedStatement preparedStatement = connection.prepareStatement("" +
                     "UPDATE simpass_user SET username=?, hash=?, role=?, credential_max_id=? WHERE id=?;")) {
            preparedStatement.setString(1, user.username());
            preparedStatement.setString(2, user.hash());
            preparedStatement.setString(3, user.role());
            preparedStatement.setLong(4, user.credentialMaxId());
            preparedStatement.setLong(5, user.id());

            return preparedStatement.executeUpdate();
        }
    }

    @Nonnull
    @Override
    public int delete(@Nonnull long id) throws SQLException {
        try (Connection connection = dataSource.getConnection();
             PreparedStatement preparedStatement = connection.prepareStatement("" +
                     "DELETE FROM simpass_user WHERE id=?;")) {
            preparedStatement.setLong(1, id);

            return preparedStatement.executeUpdate();
        }
    }
}
