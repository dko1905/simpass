package eu._0chaos.simpass.repository;

import org.springframework.stereotype.Repository;
import eu._0chaos.simpass.exception.RecordExistsException;
import eu._0chaos.simpass.model.User;

import javax.annotation.Nonnull;
import java.sql.SQLException;
import java.util.List;
import java.util.Optional;

@Repository
public interface UserRepository {
	/**
	 * Add user to repository.
	 * @param user User to add
	 * @return User with ID
	 * @throws SQLException Generic database exception
	 * @throws RecordExistsException User with same username already exists
	 */
	@Nonnull
	public User add(@Nonnull User user) throws SQLException, RecordExistsException;

	/**
	 * Find all users.
	 * @return List of users.
	 * @throws SQLException Generic database exception
	 */
	@Nonnull
	public List<User> find() throws SQLException;

	/**
	 * Find user by ID.
	 * @param id ID of user to find
	 * @return Optional that may contain the user.
	 * @throws SQLException Generic database exception
	 */
	@Nonnull
	public Optional<User> findById(long id) throws SQLException;

	/**
	 * Find user by username.
	 * @param username Username of user to find.
	 * @return Optional that may contain the user.
	 * @throws SQLException Generic database exception.
	 */
	@Nonnull
	public Optional<User> findByUsername(@Nonnull String username) throws SQLException;

	/**
	 * Update user in repository.
	 * @param user User info to update, user will be found with provided ID.
	 * @return Nr. of updated rows.
	 * @throws SQLException Generic database exception.
	 */
	@Nonnull
	public int update(@Nonnull User user) throws SQLException;

	/**
	 * Delete user in repository.
	 * @param id ID of user to delete
	 * @return Nr. of updated rows.
	 * @throws SQLException Generic database exception.
	 */
	@Nonnull
	public int delete(@Nonnull long id) throws SQLException;
}
